FROM gradle:jdk8-alpine
USER root
RUN apk add --no-cache openssh
RUN ssh-keygen -A
RUN ssh-keygen -t rsa -f ~/.ssh/id_rsa -P ""
RUN rc-update add sshd
RUN rc-status

#CMD ["/usr/sbin/sshd","-D"]
